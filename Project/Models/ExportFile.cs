﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;


namespace Project.Models
{
    public class ExportFile
    {
        
        public void CreateExcelFile(string fileName, List<user> data)
        {
            if (File.Exists(fileName)) File.Delete(fileName);
            var newFile = new FileInfo(fileName);
            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var ws = excel.Workbook.Worksheets.Add("DanhSach");
                ws.Cells[1, 1].Value = "ID";
                ws.Cells[1, 2].Value = "Tên";
                ws.Cells[1, 3].Value = "Tuổi";
                ws.Cells[1, 4].Value = "Nơi sinh";


                for (int i = 0; i < data.Count; i++)
                {
                    ws.Cells[i + 2, 1].Value = data[i].ID.ToString();
                    ws.Cells[i + 2, 2].Value = data[i].TEN.ToString();
                    ws.Cells[i + 2, 3].Value = data[i].TUOI.ToString();
                    ws.Cells[i + 2, 4].Value = data[i].NOISINH.ToString();
                }
                excel.Save();
            }
        }
    }
}