﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;
using System.Net.Mail;
using System.Configuration;

namespace Project.Controllers
{
    public class HomeController : Controller
    {
        CustomerEntities db = new CustomerEntities();
        ExportFile exports = new ExportFile();
        String sucess;
        public ActionResult Index()
        {
            List<user> lst = db.users.ToList();
            ViewBag.Sucess = sucess;
            return View(lst);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            List<user> lst = db.users.ToList();
            //using (ExcelPackage excel = new ExcelPackage())
            //{
            //    excel.Workbook.Worksheets.Add("Worksheet1");

            //    FileInfo excelFile = new FileInfo(@"D:\test.xlsx");
            //    excel.SaveAs(excelFile);
            //}


            exports.CreateExcelFile(@"D:\NewFile.xlsx", lst);
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        public ActionResult SendMail()
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(ConfigurationManager.AppSettings["MailTo"].ToString());
                //mail.CC.Add("ccid@hotmail.com");
                mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom".ToString()]);
                mail.Subject = "Feedback for Website";
                string Body = "Name: Nguyen Thanh Nhan - Send mail sucess";
                mail.Attachments.Add(new Attachment(@"D:\NewFile.xlsx")); //Hàm này dùng để lấy file đã có săn
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["UserName"].ToString(), ConfigurationManager.AppSettings["Passwork"].ToString());
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.EnableSsl = true;
                smtp.Port = 25;
                smtp.Timeout = 10000;
                smtp.Send(mail);
                sucess = "Gửi mail thành công";
                ViewBag.Sucess = "Message send successfully.";
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Xảy ra lỗi: " + ex;
            }

            return RedirectToAction("Index", "Home");

    }
    }
}