﻿using OfficeOpenXml.Core.ExcelPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExcelPakage
{
    public class ExportFile
    {
        private void CreateExcelFile(string fileName, IList<user> data)
        {
            if (File.Exists(fileName)) File.Delete(fileName);
            using (var excel = new ExcelPackage(new FileInfo(fileName)))
            {
                var ws = excel.Workbook.Worksheets.Add("Sheet1");
                ws.Cell(1, 1).Value = "ID";
                ws.Cell(1, 2).Value = "Tên";
                ws.Cell(1, 3).Value = "Tuổi";
                ws.Cell(1, 4).Value = "Nơi sinh";
                

                for (int i = 0; i < data.Count; i++)
                {
                    ws.Cell(i + 2, 1).Value = data[i].ID.ToString();
                    ws.Cell(i + 2, 2).Value = data[i].TEN.ToString();
                    ws.Cell(i + 2, 3).Value = data[i].TUOI.ToString();
                    ws.Cell(i + 2, 4).Value = data[i].NOISINH.ToString();
                }

                excel.Save();
            }
        }
    }
}
